# Chinese NER #

### Input format ###

* Input format (prefer BIOS tag scheme), with each character its label for one line. Sentences are splited with a null line.
* Tags:LOC(地名), ORG(机构名), PER(人名)
* Tag Strategy：BIO
* Split: 'space' (北 B-LOC)

### cluener result ###

* The overall performance of BERT and Lattice on dev(test):

  |  | Accuracy | Recall | F1 score |
  | ------ | ------ | ------ | ------ |
  | Bert+LSTM+CRF | 99.41 | 95.70 | 95.21 |
  | Lattice+LSTM  | 96.87 | 94.97 | 95.06 |