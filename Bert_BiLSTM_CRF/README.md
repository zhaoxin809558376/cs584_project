# Chinese NER using Bert #

BERT for Chinese NER.

### dataset list ###

* Chinese Word Vectors: https://github.com/Embedding/Chinese-Word-Vectors
* People's Daily: https://github.com/OYE93/Chinese-NLP-Corpus/tree/master/NER/People's%20Daily

### Model list? ###

* Bert + LSTM + CRF

### Requirement ###

* python = 3.6.5
* tensorflow = 1.3.1

### Run ###

* 1.Configure the environment
* 2.Open the run.py file
* 3.Adjust the parameters，do_train=true ，do_dev=true,do_test=false
* 4.Run the run.py file,Training model
* 5.do_train=false,do_dev=false,do_test=true
* 6.Run the run.py file,To begin testing