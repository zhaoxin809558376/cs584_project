import os
import codecs
import pickle
import collections

import tensorflow as tf
from bert_base.train.lstm_crf_layer import BLSTM_CRF
from tensorflow.contrib.layers.python.layers import initializers
from bert_base.train.train_helper import set_logger
from bert_base.bert import tokenization
from bert_base.bert import modeling

logger = set_logger('Enter model utlis')

class InputExample(object):
    def __init__(self, guid=None, text=None, label=None):
        self.guid = guid
        self.text = text
        self.label = label

class InputFeatures(object):

    def __init__(self, input_ids, input_mask, segment_ids, label_ids, ):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.label_ids = label_ids

def create_model(bert_config, is_training, input_ids, input_mask,
                 segment_ids, labels, num_labels, use_one_hot_embeddings,
                 dropout_rate=1.0, lstm_size=1, cell='lstm', num_layers=1):
    """
    Create the model
    :param bert_config: Bert configuration
    :param is_training: is it a training process
    :param input_ids:
    :param input_mask:
    :param segment_ids:
    :param labels: labels idx
    :param num_labels: labels numbers
    :param use_one_hot_embeddings:
    :return:
    """
    # Uncorporating Bert into the generic model
    # 1.Gets the embedding data
    # Load the BertModel with the data to get the corresponding word embedding
    model = modeling.BertModel(
        config=bert_config,
        is_training=is_training,
        input_ids=input_ids,
        input_mask=input_mask,
        token_type_ids=segment_ids,
        use_one_hot_embeddings=use_one_hot_embeddings
    )
    # Gets the corresponding Embedding input data[batch_size, seq_length, embedding_size]
    embedding = model.get_sequence_output()
    max_seq_length = embedding.shape[1].value
    # Calculate the sequence's true length
    used = tf.sign(tf.abs(input_ids))
    lengths = tf.reduce_sum(used, reduction_indices=1)  # [batch_size] 大小的向量，包含了当前batch中的序列长度
    # Add CRF Output Layer
    blstm_crf = BLSTM_CRF(
        embedded_chars=embedding,
        hidden_unit=lstm_size,
        cell_type=cell,
        num_layers=num_layers,
        dropout_rate=dropout_rate,
        initializers=initializers,
        num_labels=num_labels,
        seq_length=max_seq_length,
        labels=labels,
        lengths=lengths,
        is_training=is_training
    )

    rst = blstm_crf.add_blstm_crf_layer(crf_only=False)
    return rst

def convert_single_example(ex_index, example, label_list, max_seq_length, tokenizer, output_dir, mode):
    """
    A sample is analyzed, then words are converted to ids, labels are converted to ids, and then structured into an InputFeatures object
    :param ex_index: index
    :param example: 一个样本
    :param label_list: 标签列表
    :param max_seq_length:
    :param tokenizer:
    :param output_dir
    :param mode:
    :return:
    """
    label_map = {}
    # 1 indicates that the label is indexed from 1
    for (i, label) in enumerate(label_list, 1):
        label_map[label] = i
    # save the label->the index of the map
    if not os.path.exists(os.path.join(output_dir, 'label2id.pkl')):
        with codecs.open(os.path.join(output_dir, 'label2id.pkl'), 'wb') as w:
            pickle.dump(label_map, w)

    textlist = example.text.split(' ')
    labellist = example.label.split(' ')
    tokens = []
    labels = []
    for i, word in enumerate(textlist):
        # Word segmentation,if in chinese,is word segmentation,but for some vocab.txt characters not in Bert will be
        # WordPice processing (such as Chinese quotation marks).can replace all word segmentation operation with
        # list(input)
        token = tokenizer.tokenize(word)
        tokens.extend(token)
        label_1 = labellist[i]
        for m in range(len(token)):
            if m == 0:
                labels.append(label_1)
            else:
                labels.append("X")
    # tokens = tokenizer.tokenize(example.text)
    # Sequence truncation
    if len(tokens) >= max_seq_length - 1:
        tokens = tokens[0:(max_seq_length - 2)]# The reason for -2 is that the sequence requires a leading and ending flag
        labels = labels[0:(max_seq_length - 2)]
    ntokens = []
    segment_ids = []
    label_ids = []
    ntokens.append("[CLS]")  # The CLS flag is set at the beginning of the sentence
    segment_ids.append(0)
    # append("O") or append("[CLS]") not sure!
    label_ids.append(label_map["[CLS]"])# O OR CLS has no effect, but I think O will reduce the number of labels, but reject and use different labels at the end of sentences, and there is no problem with using LCS
    for i, token in enumerate(tokens):
        ntokens.append(token)
        segment_ids.append(0)
        label_ids.append(label_map[labels[i]])
    ntokens.append("[SEP]")  # Add a [SEP] flag at the end of the sentence
    segment_ids.append(0)
    # append("O") or append("[SEP]") not sure!
    label_ids.append(label_map["[SEP]"])
    input_ids = tokenizer.convert_tokens_to_ids(ntokens)  # Convert the words in the sequence (ntokens) to IDs
    input_mask = [1] * len(input_ids)
    # label_mask = [1] * len(input_ids)
    # padding
    while len(input_ids) < max_seq_length:
        input_ids.append(0)
        input_mask.append(0)
        segment_ids.append(0)
        # we don't concerned about it!
        label_ids.append(0)
        ntokens.append("**NULL**")
        # label_mask.append(0)
    # print(len(input_ids))
    assert len(input_ids) == max_seq_length
    assert len(input_mask) == max_seq_length
    assert len(segment_ids) == max_seq_length
    assert len(label_ids) == max_seq_length
    # assert len(label_mask) == max_seq_length

    # Print part of the sample data information
    if ex_index < 5:
        logger.info("*** Example ***")
        logger.info("guid: %s" % (example.guid))
        logger.info("tokens: %s" % " ".join(
            [tokenization.printable_text(x) for x in tokens]))
        logger.info("input_ids: %s" % " ".join([str(x) for x in input_ids]))
        logger.info("input_mask: %s" % " ".join([str(x) for x in input_mask]))
        logger.info("segment_ids: %s" % " ".join([str(x) for x in segment_ids]))
        logger.info("label_ids: %s" % " ".join([str(x) for x in label_ids]))
        # logger.info("label_mask: %s" % " ".join([str(x) for x in label_mask]))

    # Structured as a class
    feature = InputFeatures(
        input_ids=input_ids,
        input_mask=input_mask,
        segment_ids=segment_ids,
        label_ids=label_ids,
        # label_mask = label_mask
    )
    # mode='test' is effective
    write_tokens(ntokens, output_dir, mode)
    return feature

def filed_based_convert_examples_to_features(
        examples, label_list, max_seq_length, tokenizer, output_file, output_dir, mode=None):
    """
    The data is transformed into a TF_RECORD structure, which is used as the model data input
    :param examples:  sample
    :param label_list:label list
    :param max_seq_length: The preset maximum sequence length
    :param tokenizer: tokenizer object
    :param output_file: tf.record directory
    :param mode:
    :return:
    """
    writer = tf.python_io.TFRecordWriter(output_file)
    # Go through the training data
    for (ex_index, example) in enumerate(examples):
        if ex_index % 5000 == 0:
            logger.info("Writing example %d of %d" % (ex_index, len(examples)))
        # For each training sample
        feature = convert_single_example(ex_index, example, label_list, max_seq_length, tokenizer, output_dir, mode)

        def create_int_feature(values):
            f = tf.train.Feature(int64_list=tf.train.Int64List(value=list(values)))
            return f

        features = collections.OrderedDict()
        features["input_ids"] = create_int_feature(feature.input_ids)
        features["input_mask"] = create_int_feature(feature.input_mask)
        features["segment_ids"] = create_int_feature(feature.segment_ids)
        features["label_ids"] = create_int_feature(feature.label_ids)
        tf_example = tf.train.Example(features=tf.train.Features(feature=features))
        writer.write(tf_example.SerializeToString())

def file_based_input_fn_builder(input_file, seq_length, is_training, drop_remainder):
    name_to_features = {
        "input_ids": tf.FixedLenFeature([seq_length], tf.int64),
        "input_mask": tf.FixedLenFeature([seq_length], tf.int64),
        "segment_ids": tf.FixedLenFeature([seq_length], tf.int64),
        "label_ids": tf.FixedLenFeature([seq_length], tf.int64),
    }

    def _decode_record(record, name_to_features):
        example = tf.parse_single_example(record, name_to_features)
        for name in list(example.keys()):
            t = example[name]
            if t.dtype == tf.int64:
                t = tf.to_int32(t)
            example[name] = t
        return example

    def input_fn(params):
        batch_size = params["batch_size"]
        d = tf.data.TFRecordDataset(input_file)
        if is_training:
            d = d.repeat()
            d = d.shuffle(buffer_size=300)
        d = d.apply(tf.data.experimental.map_and_batch(lambda record: _decode_record(record, name_to_features),
                                                       batch_size=batch_size,
                                                       num_parallel_calls=2,
                                                       drop_remainder=drop_remainder))
        d = d.prefetch(buffer_size=4)
        return d

    return input_fn

def decode_labels(labels, batch_size):
    new_labels = []
    for row in range(batch_size):
        label = []
        for i in labels[row]:
            i = i.decode('utf-8')
            if i == '**PAD**':
                break
            if i in ['[CLS]', '[SEP]']:
                continue
            label.append(i)
        new_labels.append(label)
    return new_labels


def convert_id_str(input_ids, batch_size):
    res = []
    for row in range(batch_size):
        line = []
        for i in input_ids[row]:
            i = i.decode('utf-8')
            if i == '**PAD**':
                break
            if i in ['[CLS]', '[SEP]']:
                continue

            line.append(i)
        res.append(line)
    return res


def convert_id_to_label(pred_ids_result, idx2label, batch_size):
    """
    Converts the result in the form of ID to the real sequence result
    :param pred_ids_result:
    :param idx2label:
    :return:
    """
    result = []
    index_result = []
    for row in range(batch_size):
        curr_seq = []
        curr_idx = []
        ids = pred_ids_result[row]
        for idx, id in enumerate(ids):
            if id == 0:
                break
            curr_label = idx2label[id]
            if curr_label in ['[CLS]', '[SEP]']:
                if id == 102 and (idx < len(ids) and ids[idx + 1] == 0):
                    break
                continue
            # elif curr_label == '[SEP]':
            #     break
            curr_seq.append(curr_label)
            curr_idx.append(id)
        result.append(curr_seq)
        index_result.append(curr_idx)
    return result, index_result


def result_to_json(self, string, tags):
    """
    The model annotation sequence and the input sequence are combined into the result
    :param string: Input sequence
    :param tags: tags
    :return:
    """
    item = {"entities": []}
    entity_name = ""
    entity_start = 0
    idx = 0
    last_tag = ''

    for char, tag in zip(string, tags):
        if tag[0] == "S":
            self.append(char, idx, idx+1, tag[2:])
            item["entities"].append({"word": char, "start": idx, "end": idx+1, "type":tag[2:]})
        elif tag[0] == "B":
            if entity_name != '':
                self.append(entity_name, entity_start, idx, last_tag[2:])
                item["entities"].append({"word": entity_name, "start": entity_start, "end": idx, "type": last_tag[2:]})
                entity_name = ""
            entity_name += char
            entity_start = idx
        elif tag[0] == "I":
            entity_name += char
        elif tag[0] == "O":
            if entity_name != '':
                self.append(entity_name, entity_start, idx, last_tag[2:])
                item["entities"].append({"word": entity_name, "start": entity_start, "end": idx, "type": last_tag[2:]})
                entity_name = ""
        else:
            entity_name = ""
        entity_start = idx
        idx += 1
        last_tag = tag
    if entity_name != '':
        self.append(entity_name, entity_start, idx, last_tag[2:])
        item["entities"].append({"word": entity_name, "start": entity_start, "end": idx, "type": last_tag[2:]})
    return item

def write_tokens(tokens, output_dir, mode):
    """
    Writes the sequence parsing results to a file
    Enable only when mode=test
    :param tokens:
    :param mode:
    :return:
    """
    if mode == "test":
        path = os.path.join(output_dir, "token_" + mode + ".txt")
        wf = codecs.open(path, 'a', encoding='utf-8')
        for token in tokens:
            if token != "**NULL**":
                wf.write(token + '\n')
        wf.close()
